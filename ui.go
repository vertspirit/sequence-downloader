package main

import (
  "log"
  "strings"
  "regexp"
  "strconv"
  "os"

  "github.com/therecipe/qt/widgets"

  "sequence-downloader/pkg/downloader"
  "sequence-downloader/pkg/gui"
)

type WidgetSettings = gui.WidgetSettings
type GuiDownloadTableView = downloader.GuiDownloadTableView

func StartUI() {
  gui.EnableHighDpiScaling()
  app := gui.CreateQApp()

  mw := gui.CreateMainWindow("Sequence Downloader", "resources/images/logo.svg", 800, 600, 480, 280, 0, 0)
  gui.CenterMainWindow2(mw)
  mw.ConnectCloseEvent(gui.MainWindowCloseEvent)

  baseSettings := &gui.WidgetSettings{}
  // basic layout widget with vertical layout to contain other elements
  basebox, vboxlayout, err := baseSettings.CreateBoxLayout(mw, "vertical", 0)
  if err != nil {
    log.Printf("Fail to create box layout: %s\n", err)
  }
  vbox := gui.BoxLayoutToVBoxLayout(vboxlayout)
  vbox.SetContentsMargins(1, 2, 1, 2)
  mw.SetCentralWidget(basebox)

  // menubar
  mbar := gui.CreateMebuBar(basebox)
  MenuFile(mbar, mw)
  MenuHelp(mbar)
  vbox.AddWidget(mbar, 0, 32)

  // http scheme options
  schemeHbox := widgets.NewQHBoxLayout2(basebox)
  schemeHbox.SetContentsMargins(1, 1, 1, 1)
  vbox.AddLayout(schemeHbox, 0)
  vbox.SetAlignment2(schemeHbox, 32)
  schemeLabel := baseSettings.CreateLabel(basebox, 0, "Scheme:", "")
  schemeLabel.SetMargin(0)
  schemeLabel.SetIndent(3)
  schemeComboBox := gui.CreateComboBox(basebox, "https", "http")
  mergeCheckBox := gui.CreateCheckBox(basebox, "Merge TS Files")
  schemeHbox.AddSpacing(12)
  schemeHbox.AddWidget(schemeLabel, 0, 1)
  schemeHbox.AddSpacing(8)
  schemeHbox.AddWidget(schemeComboBox, 0, 1)
  // spacer := widgets.NewQSpacerItem(350, 0, 1, 0)
  // schemeHbox.AddSpacerItem(spacer)
  schemeHbox.AddStretch(1)
  schemeHbox.AddWidget(mergeCheckBox, 0, 1)
  schemeHbox.AddSpacing(12)

  // sequnce downloading settings
  seqVbox := widgets.NewQVBoxLayout2(basebox)
  seqVbox.SetContentsMargins(1, 1, 1, 1)
  vbox.AddLayout(seqVbox, 1)
  vbox.SetAlignment2(seqVbox, 32)

  prefixHbox := widgets.NewQHBoxLayout2(basebox)
  prefixHbox.SetContentsMargins(1, 1, 1, 1)
  prefixLabel := baseSettings.CreateLabel(basebox, 0, "Sequence Prefix:", "")
  prefixLabel.SetMargin(0)
  prefixLabel.SetIndent(12)
  prefixInput := widgets.NewQLineEdit(basebox)
  prefixInput.SetAlignment(1)
  prefixInput.SetTextMargins(1, 1, 1, 1)
  prefixInput.SetPlaceholderText("https://xxx.xxx.xx.xxx/yyy-zzz-")
  prefixInput.CreateStandardContextMenu()
  prefixInput.SetMinimumWidth(340)
  policy := widgets.NewQSizePolicy()
  policy.SetHorizontalStretch(1)
  prefixInput.SetSizePolicy(policy)
  prefixHbox.AddWidget(prefixLabel, 0, 1)
  prefixHbox.AddWidget(prefixInput, 3, 1)
  suffixHbox := widgets.NewQHBoxLayout2(basebox)
  suffixHbox.SetContentsMargins(1, 1, 1, 1)
  suffixLabel := baseSettings.CreateLabel(basebox, 0, "Sequence Suffix:", "")
  suffixLabel.SetMargin(0)
  suffixLabel.SetIndent(12)
  suffixInput := widgets.NewQLineEdit(basebox)
  suffixInput.SetAlignment(1)
  suffixInput.SetTextMargins(1, 1, 1, 1)
  suffixInput.SetPlaceholderText(".ts")
  suffixInput.CreateStandardContextMenu()
  suffixInput.SetMinimumWidth(340)
  suffixInput.SetSizePolicy(policy)
  suffixHbox.AddWidget(suffixLabel, 0, 1)
  suffixHbox.AddWidget(suffixInput, 3, 1)
  seqVbox.AddLayout(prefixHbox, 0)
  seqVbox.SetAlignment2(prefixHbox, 32)
  seqVbox.AddLayout(suffixHbox, 0)
  seqVbox.SetAlignment2(suffixHbox, 32)

  rangeHbox := widgets.NewQHBoxLayout2(basebox)
  rangeHbox.SetContentsMargins(1, 1, 1, 1)
  fitCheckBox := gui.CreateCheckBox(basebox, "Left Pad")
  extensionLaberl := baseSettings.CreateLabel(basebox, 0, "Extension:", "")
  extensionInput := widgets.NewQLineEdit2(".ts", basebox)
  extensionInput.SetAlignment(1)
  extensionInput.SetTextMargins(1, 1, 1, 1)
  extensionInput.SetMinimumWidth(30)
  extensionInput.SetFixedWidth(40)
  startLabel := baseSettings.CreateLabel(basebox, 0, "Start:", "")
  startSpin := gui.CreateSpinBox(basebox, 0, 9999, 1, 0, "", "")
  endLabel := baseSettings.CreateLabel(basebox, 0, "End:", "")
  endSpin := gui.CreateSpinBox(basebox, 0, 99999, 1, 100, "", "")
  startSpin.ConnectValueChanged(func(val int) {
    if startSpin.Value() > endSpin.Value() {
      startSpin.SetValue(endSpin.Value())
    }
  })
  endSpin.ConnectValueChanged(func(val int) {
    if endSpin.Value() < startSpin.Value() {
      endSpin.SetValue(startSpin.Value())
    }
  })
  rangeHbox.AddSpacing(12)
  rangeHbox.AddWidget(fitCheckBox, 0, 1)
  rangeHbox.AddSpacing(8)
  rangeHbox.AddWidget(startLabel, 0, 1)
  rangeHbox.AddWidget(startSpin, 0, 1)
  rangeHbox.AddWidget(endLabel, 0, 1)
  rangeHbox.AddWidget(endSpin, 0, 1)
  rangeHbox.AddStretch(1)
  rangeHbox.AddWidget(extensionLaberl, 0, 1)
  rangeHbox.AddWidget(extensionInput, 0, 1)
  rangeHbox.AddSpacing(12)
  seqVbox.AddLayout(rangeHbox, 1)
  seqVbox.SetAlignment2(rangeHbox, 32)

  leftPadGpBox := widgets.NewQGroupBox(basebox)
  leftPadGpBox.SetContentsMargins(1, 1, 1, 1)
  leftPadGpBox.SetTitle("    Left Pad Options")
  leftPadGpBox.SetFlat(true)
  leftPadGpBox.SetHiddenDefault(true)
  leftPadOptsHbox := widgets.NewQHBoxLayout2(basebox)
  leftPadOptsHbox.SetContentsMargins(1, 1, 1, 1)
  mwsize := mw.Size() // core.NewQSize()
  fitCheckBox.ConnectStateChanged(func(state int) {
    if state == 2 {
      leftPadGpBox.SetHidden(false)
      mw.AdjustSize()
    } else {
      leftPadGpBox.SetHidden(true)
      mw.Resize(mwsize) // Resize2(w int, h int)
    }
  })
  padsymbolLabel := baseSettings.CreateLabel(basebox, 0, "Pad:", "")
  padsymbolLabel.SetMargin(0)
  padsymbolLabel.SetIndent(12)
  padsymbolInput := widgets.NewQLineEdit2("0", basebox)
  padsymbolInput.SetAlignment(1)
  padsymbolInput.SetTextMargins(1, 1, 1, 1)
  padsymbolInput.SetMaxLength(1)
  padsymbolInput.SetMinimumWidth(10)
  regvder := gui.CreateRegExpValidator(nil, "[a-zA-Z0-9]+")
  padsymbolInput.SetValidator(regvder)
  padnumberLabel := baseSettings.CreateLabel(basebox, 0, "Digits:", "")
  padnumberLabel.SetMargin(0)
  padnumberLabel.SetIndent(12)
  padnumberInput := widgets.NewQLineEdit2("5", basebox)
  padnumberInput.SetAlignment(1)
  padnumberInput.SetTextMargins(1, 1, 1, 1)
  padnumberInput.SetMinimumWidth(15)
  intvder := gui.CreateNewQIntValidator(nil, 0, 0)
  padnumberInput.SetValidator(intvder)
  leftPadOptsHbox.AddSpacing(12)
  leftPadOptsHbox.AddWidget(padsymbolLabel, 0, 1)
  leftPadOptsHbox.AddSpacing(8)
  leftPadOptsHbox.AddWidget(padsymbolInput, 0, 1)
  leftPadOptsHbox.AddSpacing(16)
  leftPadOptsHbox.AddWidget(padnumberLabel, 0, 1)
  leftPadOptsHbox.AddSpacing(8)
  leftPadOptsHbox.AddWidget(padnumberInput, 0, 1)
  leftPadOptsHbox.AddSpacing(12)
  leftPadGpBox.SetLayout(leftPadOptsHbox)
  vbox.AddWidget(leftPadGpBox, 0, 32)

  savedHBox := widgets.NewQHBoxLayout2(basebox)
  savedHBox.SetContentsMargins(1, 1, 1, 1)
  vbox.AddLayout(savedHBox, 1)
  vbox.SetAlignment2(savedHBox, 32)
  locationLebal := baseSettings.CreateLabel(basebox, 0, "Location to save:", "")
  locationLebal.SetMargin(0)
  locationLebal.SetIndent(12)
  locationInput := widgets.NewQLineEdit(basebox)
  locationInput.SetAlignment(1)
  locationInput.SetTextMargins(1, 1, 1, 1)
  locationInput.SetMinimumWidth(250)
  locationInput.SetDragEnabled(true)
  locationInput.SetPlaceholderText("incoming")
  locationButton := gui.CreatePushButton(basebox, "Browse", func(bool) {
    path := widgets.QFileDialog_GetExistingDirectory(basebox, "Select a directory to save", "./", 1)
    locationInput.SetText(path)
  })
  savedHBox.AddWidget(locationLebal, 0, 1)
  savedHBox.AddSpacing(8)
  savedHBox.AddWidget(locationInput, 3, 1)
  savedHBox.AddSpacing(8)
  savedHBox.AddWidget(locationButton, 0, 1)
  savedHBox.AddSpacing(12)

  runHBox := widgets.NewQHBoxLayout2(basebox)
  runHBox.SetContentsMargins(1, 1, 1, 1)
  vbox.AddLayout(runHBox, 1)
  vbox.SetAlignment2(runHBox, 32)
  concurrentLebal := baseSettings.CreateLabel(basebox, 0, "Simultaneous Downloads:", "")
  concurrentLebal.SetMargin(0)
  concurrentLebal.SetIndent(12)
  concurrentInput := widgets.NewQLineEdit(basebox)
  concurrentInput.SetAlignment(1)
  concurrentInput.SetTextMargins(1, 1, 1, 1)
  concurrentInput.SetTextDefault("3")
  concurrentInput.SetMinimumWidth(30)
  concurrentInput.SetFixedWidth(40)
  intvder2 := gui.CreateNewQIntValidator(nil, 1, 99)
  concurrentInput.SetValidator(intvder2)
  timeoutLebal := baseSettings.CreateLabel(basebox, 0, "Timeout:", "")
  timeoutInput := widgets.NewQLineEdit(basebox)
  timeoutInput.SetAlignment(1)
  timeoutInput.SetTextMargins(1, 1, 1, 1)
  timeoutInput.SetTextDefault("30")
  timeoutInput.SetMinimumWidth(40)
  timeoutInput.SetFixedWidth(40)
  intvder3 := gui.CreateNewQIntValidator(nil, 0, 0)
  timeoutInput.SetValidator(intvder3)
  runButton := widgets.NewQPushButton2("Download", basebox)
  runButton.ConnectClicked(func(bool) {
    // check the required values
    if prefixInput.Text() == "" || suffixInput.Text() == "" {
      msgdlg := &gui.WidgetSettings{
        Text: "The settings for prefix and suffix fields are required!",
      }
      msgdlg.CreateInfoMessageBox(basebox)
    } else {
      var (
        dir  string
        pad  int
      )
      if locationInput.Text() == "" {
        dir = "incoming"
      } else {
        dir = locationInput.Text()
      }
      _, err := os.Stat(dir)
      if os.IsNotExist(err) {
        os.MkdirAll(dir ,0755)
      }
      if fitCheckBox.CheckState() == 2 {
        pad, _ = strconv.Atoi(padnumberInput.Text())
      } else {
        pad = 0
      }
      cocunr, _ := strconv.Atoi(concurrentInput.Text())
      timeout, _ := strconv.Atoi(timeoutInput.Text())
      prefix := TrimUrlPrefix(schemeComboBox.CurrentText(), prefixInput.Text())
      guitv := &GuiDownloadTableView {
        Widget:      basebox,
        Directory:   dir,
        Prefix:      prefix,
        Suffix:      suffixInput.Text(),
        Extension:   extensionInput.Text(),
        PadChar:     padsymbolInput.Text(),
        Pads:        pad,
        StartValue:  startSpin.Value(),
        EndValue:    endSpin.Value(),
        StepValue:   1,
        Works:       cocunr,
        Timeout:     timeout,
      }
      guitv.GuiDownloadTableView()
    }
  })
  runHBox.AddWidget(concurrentLebal, 0, 1)
  runHBox.AddSpacing(8)
  runHBox.AddWidget(concurrentInput, 0, 1)
  runHBox.AddSpacing(8)
  runHBox.AddWidget(timeoutLebal, 0, 1)
  runHBox.AddSpacing(8)
  runHBox.AddWidget(timeoutInput, 0, 1)
  runHBox.AddStretch(1)
  runHBox.AddWidget(runButton, 0, 1)
  runHBox.AddSpacing(12)

  mw.Show()
  app.Exec()
}

func MenuFile(mbar *widgets.QMenuBar, mw *widgets.QMainWindow) {
  menu := gui.CreateMenu(mbar, "&Action", "")
  gui.AddMenuAction(menu, "&exit", "", "Ctrl+Q", "Exit this application", func(bool){
    mw.Close()
  })
}

func MenuHelp(mbar *widgets.QMenuBar) {
  menu := gui.CreateMenu(mbar, "&Help", "")
  gui.AddMenuAction(menu, "&about", "", "", "show informations for this application", func(popout bool){
    a := &WidgetSettings{
      Title: "About",
      Text: "A simple gui tool for sequence downloader written in golang and qt toolkit.",
    }
    a.CreateAboutMessageBox(menu)
  })
  gui.AddMenuAction(menu, "about &Qt", "", "", "show informations for Qt", func(popout bool){
    aq := &WidgetSettings{
      Title: "About Qt",
    }
    aq.CreateAboutQtMessageBox(menu)
  })
}

func TrimUrlPrefix(scheme, prefix string) string {
  var result string
  pfx := strings.TrimSpace(prefix)
  re := regexp.MustCompile("^" + scheme + "://.*")
  if ! re.MatchString(pfx) {
    replacer := strings.NewReplacer("http://", "", "https://", "")
    result = replacer.Replace(pfx)
    result = scheme + "://" + result
  } else {
    result = strings.TrimSpace(prefix)
  }
  return result
}
