package logger

import (
  "fmt"
  "log"
  "os"
)

type FileLogger struct {
  File  string
  Name  string
}

func (f *FileLogger) Write(message []byte) (int, error) {
  flags := os.O_APPEND | os.O_WRONLY | os.O_CREATE
  mode := os.FileMode(0644)
  logfile, err := os.OpenFile(f.File, flags, mode)
  if err != nil {
    log.Printf("Fail to open log file %s: %v\n", f.File, err)
    return 1, err
  }
  defer logfile.Close()

  // _, err = fmt.Fprintln(logfile, *(*string)(unsafe.Pointer(&message)))
  _, err = logfile.Write(message)
  if err != nil {
    fmt.Printf("Fail to write log to file %s: %s\n", f.File, err)
    return 1, err
  }

  return 0, nil
}

func (f *FileLogger) Setup() {
  log.SetPrefix(fmt.Sprintf("[%s] ", f.Name))
  log.SetFlags(log.LstdFlags | log.Lshortfile)
  log.SetOutput(f)
}

// logger := &FileLogger{ "history.log", "app-name" }
// flogger := logger.NewLooger()
// flogger.Println("Error message.")
func (f *FileLogger) NewLooger() *log.Logger {
  flags := log.LstdFlags | log.Lshortfile
  logger := log.New(f, fmt.Sprintf("[%s] ", f.Name), flags)
  return logger
}
