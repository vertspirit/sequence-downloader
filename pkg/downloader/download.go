package downloader

import (
  "fmt"
  "log"
  "os"
  "io"
  "net/http"
  "time"
  "path/filepath"
  "strconv"
  "math/rand"
  "unsafe"

  "github.com/therecipe/qt/widgets"
  "github.com/therecipe/qt/core"
  "github.com/therecipe/qt/gui"
)

type Job struct {
  URL      string
  File     string
  Index    int
  ItemId   int
  Timeout  int
}

func Worker(index int, jobs <-chan Job, finished chan<- int) {
  for job := range jobs {
    e := 0
    filename := filepath.Base(job.File)
    fmt.Printf("[Worker #%d] downloading job: %d - %s\n", index, job.Index, filename)
    size, err := ReceiveFileInfo(job.URL, job.Timeout)
    if err != nil {
      fmt.Printf("[Worker #%d] fail to get file size: %d - %s\n", index, job.Index, filename)
      e = 2
    }
    if e != 2 {
      fmt.Printf("+++ downloading file from: %s\n", job.URL)
      err := DownloadFile(job.URL, job.File, job.Timeout, int64(size))
      if err != nil {
        fmt.Printf("[Worker #%d] fail to download file: %d - %s\n", index, job.Index, filename)
        e = 1
      } else {
        fmt.Printf("[Worker #%d] finished job: %d - %s\n", index, job.Index, filename)
      }
    }
    finished <- e
  }
}

func ReceiveFileInfo(url string, timeout int) (uint64, error) {
  client := http.Client{
    Timeout: time.Duration(timeout) * time.Second,
  }

  resp, err := client.Head(url)
  if err != nil {
    log.Println("Fail to request with HEAD method:", err)
    return 0, err
  }

  switch resp.StatusCode {
  case 200:
    // for debug
    // fmt.Printf("The length of content: %v\n", resp.ContentLength) // Content-Length
    // fmt.Printf("The type of content: %s\n", resp.Header.Get("content-type")) // Content-Type
    return uint64(resp.ContentLength), nil
  default:
    sc := "[Status Code " + strconv.Itoa(resp.StatusCode) + " ]:"
    log.Println(sc, err)
    return 0, err
  }
}

func DownloadFile(url, file string, timeout int, size int64) error {
  client := http.Client{
    Timeout: time.Duration(timeout) * time.Second,
  }

  resp, err := client.Get(url)
  if err != nil {
    log.Println("Http request failed:", err)
    return err
  }
  defer resp.Body.Close()

  dir, _ := filepath.Split(file)
  // create the tmp file with random name
  tmpName := GenerateRandomString(16, false, false) + ".tmp"
  tmpPath := filepath.Join(dir, tmpName)

  output, err := os.Create(tmpPath)
  if err != nil {
    log.Println("Failed to create a file to save the downloaded content:", err)
    return err
  }
  defer output.Close()

  switch resp.StatusCode {
  case 200:
    if size == -1 {
      filename := filepath.Base(file)
      if CountCharacters(filename) > 15 {
        filename = ShotenString(filename , 15)
      }
      tp := &TextProgress{ Filename: filename }
      if _, err = io.Copy(output, io.TeeReader(resp.Body, tp)); err != nil {
        log.Println("Fail to save the response data to the file:", err)
        return err
      }
      tp.Close()
    } else {
      pv := &ProgressBar{}
      pv.New(size)
      if _, err = io.Copy(output, io.TeeReader(resp.Body, pv)); err != nil {
        log.Println("Fail to save the response data to the file:", err)
        return err
      }
      pv.Close()
    }
    err := os.Rename(tmpPath, file)
    if err != nil {
      log.Println("Fail to rename tmp file:", err)
      return err
    }
  default:
    sc := "[Status Code " + strconv.Itoa(resp.StatusCode) + " ]:"
    log.Println(sc, err)
    return err
  }

  return nil
}

func BatchDownloadFiles(dir, prefix, ext string, works, timeout int, urls ...string) {
  number := len(urls)
  fmt.Println("The total urls to download:", number)
  if number == 0 {
    log.Println("No url to downlaod.")
    return
  }
  if prefix == "random" {
    prefix = GenerateRandomString(6, false, false)
  }

  // the capacity of buffered channel need to be larger than total works, or job will be blocked.
  // It is more safety that capacity is equal to total jobs.
  jobs := make(chan Job, number)
  finished := make(chan int, number)

  // worker pool
  for n := 1; n <= works; n++ {
    go Worker(n, jobs, finished)
  }

  // allocate jobs
  for i := 0; i < number; i++ {
    digits := CountDigits(number)
    suffix := LeftPad(i + 1, digits)
    filename := prefix + "-" + suffix + "." + ext
    file := filepath.Join(dir, filename)
    dl := Job {
      URL: urls[i],
      File: file,
      Index: i + 1,
      Timeout: timeout,
    }
    jobs <- dl
  }
  close(jobs)

  for j := 0; j < number; j ++ {
    <-finished
  }
}

func SerialDownloadFiles(dir, urlPrefix, urlSuffix string, seqStart, seqEnd, seqStep, pad, works, timeout int) {
  capacity := (seqEnd - seqStart) + 1
  jobs := make(chan Job, capacity)
  finished := make(chan int, capacity)

  for n := 1; n <= works; n++ {
    go Worker(n, jobs, finished)
  }

  for i := seqStart; i <= seqEnd; i += seqStep {
    serial := LeftPad(i, pad)
    url := urlPrefix + serial + urlSuffix
    numb := LeftPad(i, 8)
    name := "TSDL-" + numb + ".ts"
    file := filepath.Join(dir, name)
    dl := Job {
      URL: url,
      File: file,
      Index: i,
      Timeout: timeout,
    }
    jobs <- dl
  }
  close(jobs)

  for j := seqStart; j <= seqEnd; j += seqStep {
    <-finished
  }
}

type GuiDownloadTableView struct {
  Widget          widgets.QWidget_ITF
  TableView       *widgets.QTableView
  ItemModel       *gui.QStandardItemModel
  ProgressBar     *widgets.QProgressBar
  ProgressDialog  *widgets.QProgressDialog
  Directory       string
  Prefix          string
  Suffix          string
  Extension       string
  PadChar         string
  Pads            int
  StartValue      int
  EndValue        int
  StepValue       int
  Works           int
  Progress        int
  Timeout         int
}

func (gtv *GuiDownloadTableView) GuiDownloadTableView() {
  capacity := (gtv.EndValue - gtv.StartValue) + 1
  if gtv.Extension == "" {
    gtv.Extension = ".ts"
  }
  dialog := widgets.NewQDialog(gtv.Widget, core.Qt__Tool)
  dialog.SetFixedSize2(640, 240) // SetFixedSize2(800, 320), SetBaseSize2(640, 320)
  dialog.SetModal(true) // SetWindowModality(core.Qt__WindowModal), core.Qt__ApplicationModal
  dialog.Show()
  dialog.ConnectCloseEvent(func(event *gui.QCloseEvent) {
    mbox := widgets.QMessageBox_Question(
      nil,
      "Stop",
      "Do you want to stop jobs now?",
      widgets.QMessageBox__Yes|widgets.QMessageBox__No,
      widgets.QMessageBox__Yes,
    )
    if mbox == widgets.QMessageBox__Yes {
      event.Accept()
    } else {
      event.Ignore()
    }
  })
  dialogVBox := widgets.NewQVBoxLayout2(dialog)
  dialog.SetLayout(dialogVBox)
  gtv.TableView = widgets.NewQTableView(dialog)
  dialogVBox.AddWidget(gtv.TableView, 1, 32)
  gtv.ItemModel = gui.NewQStandardItemModel2(capacity, 6, gtv.TableView) // gui.NewQStandardItemModel(jobTableview)
  gtv.TableView.SetModel(gtv.ItemModel)
  gtv.TableView.SetShowGridDefault(true)
  // gtv.TableView.ResizeColumnsToContentsDefault()
  // gtv.TableView.ResizeRowsToContentsDefault()
  headerdatas := []string{"Job", "Worker", "Filename", "Status", "URL to download"}
  gtv.ItemModel.SetHorizontalHeaderLabels(headerdatas) // SetHeaderDataDefault(0, core.Qt__Horizontal, core.NewQVariant17(headerdata), 0)
  row := 0
  for i := gtv.StartValue; i <= gtv.EndValue; i += gtv.StepValue {
    serial := LeftPadWithCustomCharacter(gtv.PadChar, i, gtv.Pads)
    url := gtv.Prefix + serial + gtv.Suffix
    numb := LeftPadWithCustomCharacter(gtv.PadChar, i, 8)
    name := "TSDL-" + numb + gtv.Extension
    gtv.ItemModel.SetItem(row, 0, gui.NewQStandardItem2(strconv.Itoa(row)))
    gtv.ItemModel.SetItem(row, 1, gui.NewQStandardItem2("0"))
    gtv.ItemModel.SetItem(row, 2, gui.NewQStandardItem2(name))
    gtv.ItemModel.SetItem(row, 3, gui.NewQStandardItem2("pending"))
    gtv.ItemModel.SetItem(row, 4, gui.NewQStandardItem2(url))
    // // SetDragEnabled(true), SetDropEnabled(true), SetEditable(true)
    gtv.TableView.ResizeRowsToContents()
    gtv.TableView.ResizeColumnsToContents()
    row ++
  }

  gtv.ProgressBar = widgets.NewQProgressBar(dialog)
  gtv.ProgressBar.SetOrientation(core.Qt__Horizontal)
  gtv.ProgressBar.SetRange(0, 100) // SetRange(0, 0) => busy anime
  gtv.ProgressBar.SetValue(0)
  gtv.ProgressBar.SetHidden(true)

  btnHBox := widgets.NewQHBoxLayout2(dialog)
  dialogVBox.AddLayout(btnHBox, 0)
  startBtn := widgets.NewQPushButton2("Start", dialog)
  startBtn.ConnectClicked(func(bool) {
    go func(){
      gtv.ProgressDialog = widgets.NewQProgressDialog(gtv.Widget, core.Qt__Tool)
      gtv.ProgressDialog.SetWindowModality(core.Qt__ApplicationModal)
      gtv.ProgressDialog.SetMinimumDuration(2)
      gtv.ProgressDialog.SetLabelText("Downloading files...")
      gtv.ProgressDialog.SetRange(0, 0)
      gtv.ProgressDialog.SetValue(0)
      for {
        time.Sleep(1)
        if gtv.Progress >= 99 {
          gtv.ProgressDialog.Close()
          break
        }
      }
    }()
    go gtv.ProgressBar.SetHidden(false)
    go gtv.GuiSequenceDownload()
  })
  closeBtn := widgets.NewQPushButton2("Close", dialog)
  closeBtn.ConnectClicked(func(bool) {
    dialog.DestroyQWidget()
  })

  btnHBox.AddWidget(startBtn, 0, 1)
  btnHBox.AddSpacing(8)
  btnHBox.AddWidget(gtv.ProgressBar, 0, 1)
  btnHBox.AddStretch(1)
  btnHBox.AddWidget(closeBtn, 0, 1)
}

func (gtv *GuiDownloadTableView) ModelItemUpdate(mi *core.QModelIndex, row, column int) *core.QModelIndex {
  // modelIndex := core.NewQModelIndex()
  return gtv.ItemModel.Index(row, column, mi)
}

func (gtv *GuiDownloadTableView) GuiWorker(index int, jobs <-chan Job, finished chan<- int) {
  for job := range jobs {
    e := 0
    gtv.ItemModel.SetItem(job.ItemId, 1, gui.NewQStandardItem2(strconv.Itoa(index)))
    size, err := ReceiveFileInfo(job.URL, job.Timeout)
    if err != nil {
      gtv.ItemModel.SetItem(job.ItemId, 3, gui.NewQStandardItem2("failed"))
      e = 2
    }
    if e != 2 {
      err := DownloadFile(job.URL, job.File, job.Timeout, int64(size))
      if err != nil {
        gtv.ItemModel.SetItem(job.ItemId, 3, gui.NewQStandardItem2("failed"))
        e = 1
      } else {
        gtv.ItemModel.SetItem(job.ItemId, 3, gui.NewQStandardItem2("finished"))
      }
    }
    finished <- e
  }
}

func (gtv *GuiDownloadTableView) GuiSequenceDownload() {
    capacity := (gtv.EndValue - gtv.StartValue) + 1
    jobs := make(chan Job, capacity)
    finished := make(chan int, capacity)

    if gtv.Extension == "" {
      gtv.Extension = ".ts"
    }

    for n := 1; n <= gtv.Works; n++ {
      go gtv.GuiWorker(n, jobs, finished)
    }

    itemId := 0
    for i := gtv.StartValue; i <= gtv.EndValue; i += gtv.StepValue {
      serial := LeftPadWithCustomCharacter(gtv.PadChar, i, gtv.Pads)
      url := gtv.Prefix + serial + gtv.Suffix
      numb := LeftPadWithCustomCharacter(gtv.PadChar, i, 8)
      name := "TSDL-" + numb + gtv.Extension
      file := filepath.Join(gtv.Directory, name)
      dl := Job {
        URL:     url,
        File:    file,
        Index:   i,
        ItemId:  itemId,
        Timeout: gtv.Timeout,
      }
      jobs <- dl
      itemId ++
    }
    close(jobs)

    finishedJobs := 0
    for j := gtv.StartValue; j <= gtv.EndValue; j += gtv.StepValue {
      <-finished
      finishedJobs += 1
      percent := (finishedJobs / capacity) * 100
      gtv.Progress = percent
      gtv.ProgressBar.SetValue(gtv.Progress)
    }
}

func CountCharacters(str string) int {
  len := 0
  for _, _ = range str {
    len++
  }
  return len
}

func CountDigits(number int) int {
  digits := 0
  for number != 0 {
    number /= 10
    digits += 1
  }
  if digits == 0 {
    digits = 1
  }
  return digits
}

func LeftPad(num int, leng int) string {
  s := strconv.Itoa(num)
  for n := len(s); len(s) < leng; n++ {
    s = "0" + s
  }
  return s
}

func LeftPadWithCustomCharacter(char string, num int, leng int) string {
  if char == "" {
    char = "0"
  }
  s := strconv.Itoa(num)
  for n := len(s); len(s) < leng; n++ {
    s = char + s
  }
  return s
}

func ShotenString(s string, l int) string {
  return fmt.Sprintf(fmt.Sprintf("%%%d.%ds", l, l), s)
}

func GenerateRandomString(length int, upper, special bool) string {
  digits := "0123456789"
  lowercase := "abcdefghijklmnopqrstuvwxyz"
  uppercase := "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
  specials := "~=+%^*/()[]{}/!@#$?|"
  characters := digits + lowercase

  if upper {
    characters += uppercase
  }
  if special {
    characters += specials
  }

  rand.Seed(time.Now().UnixNano())
  buf := make([]byte, length)
  for i := 0; i < length; i++ {
    buf[i] = characters[rand.Intn(len(characters))]
  }

  return *(*string)(unsafe.Pointer(&buf))
}
