package downloader

import (
  "fmt"
  "strings"
  "strconv"
  "math"
)

type ProgressBar struct {
  Mark   string
  Arrow  string
  Total  int64
}

func (pbar *ProgressBar) New(total int64) {
  if pbar.Mark == "" && pbar.Arrow == "" {
    pbar.Mark  = "=" // "█", "■"
    pbar.Arrow = ">"
  }
  pbar.Total = total
}

func (pbar *ProgressBar) Draw(current int64) {
  percent := int64((float32(current) / float32(pbar.Total)) * 100)
  process := int(percent / 2)
  fmt.Printf("\r%s", strings.Repeat(" ", 100)) // remove message and go back to start position
  if percent >= 100 {
    fmt.Printf("\r[%-50s] %3d%% %8d/%d", strings.Repeat(pbar.Mark, process), percent, current, pbar.Total)
  } else if percent == 0 {
    fmt.Printf("\r[%-50s] %3d%% %8d/%d", "", percent, current, pbar.Total)
  } else {
    fmt.Printf("\r[%-50s] %3d%% %8d/%d", strings.Repeat(pbar.Mark, process) + pbar.Arrow, percent, current, pbar.Total)
  }
}

func (pbar *ProgressBar) Write(data []byte) (int, error) {
  pbar.Draw(int64(len(data)))
  return 0, nil
}

func (pbar *ProgressBar) Close(){
  // the progress bar display in the same line, so need to print the new line at end
  fmt.Println()
}

// simple text info for downloading in the console
type TextProgress struct {
  Filename  string
  Total     int64
}

func (tp *TextProgress) Write(data []byte) (int, error) {
  tp.Total += int64(len(data))
  // remove message and go back to start position by using enter character
  fmt.Printf("\r%s", strings.Repeat(" ", 80))
  fmt.Printf("\rDownloading file: %s ...... %s complete", tp.Filename, tp.CalculateSize(tp.Total, true))
  return 0, nil
}

func (tp *TextProgress) Close(){
  fmt.Println()
}

func (tp *TextProgress) CalculateSize(b int64, si bool) string {
  var size string
  switch {
  case b < 1000 :
    size = fmt.Sprintf("%s %s", strconv.FormatInt(b, 10), "Bytes")
  case b >= 1000 && b < 1000000:
    kb := strconv.FormatFloat(math.Floor(((float64(b) / 1000.0) * 100) / 100), 'f', 2, 64)
    size = fmt.Sprintf("%s %s", kb, "KB")
  case b >= 1000000 && b < 1000000000:
    mb := strconv.FormatFloat(math.Floor(((float64(b) / 1000000.0) * 100) / 100), 'f', 2, 64)
    size = fmt.Sprintf("%s %s", mb, "MB")
  default:
    gb := strconv.FormatFloat(math.Floor(((float64(b) / 1000000000.0) * 100) / 100), 'f', 2, 64)
    size = fmt.Sprintf("%s %s", gb, "GB")
  }
  return size
}
