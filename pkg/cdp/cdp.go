package cdp

import(
  "fmt"
  "context"
  "time"
  "regexp"

  "github.com/chromedp/chromedp"
  "github.com/chromedp/cdproto/network"
)

func ListenNetworkEvents(ctx context.Context) {
  chromedp.ListenTarget(ctx, func(ev interface{}) {
    switch ev.(type) {
    case *network.EventRequestWillBeSent:
      fmt.Println("event received (EventRequestWillBeSent):")
      fmt.Println(ev.(*network.EventRequestWillBeSent).Request.URL)
      fmt.Println(ev.(*network.EventRequestWillBeSent).Request.Headers)
    case *network.EventResponseReceived:
      fmt.Println("event received (EventResponseReceived):")
      fmt.Println("URL:", ev.(*network.EventResponseReceived).Response.URL)
      fmt.Println("STATUS CODE:", ev.(*network.EventResponseReceived).Response.Status)
      fmt.Println("STATUS:", ev.(*network.EventResponseReceived).Response.StatusText)
      fmt.Println("Headers:", ev.(*network.EventResponseReceived).Response.Headers)
    default:
    }
  })
}

func GetRequestURLs(ctx context.Context, urls *[]string, filter bool, reg string) {
  chromedp.ListenTarget(ctx, func(ev interface {}) {
    event, ok := ev.(*network.EventRequestWillBeSent)
    if ok {
      fmt.Println(event.Request.URL)
      if filter {
        matched, _ := regexp.MatchString(reg, event.Request.URL)
        if matched {
          *urls = append(*urls, event.Request.URL)
        }
      } else {
        *urls = append(*urls, event.Request.URL)
      }
    }
  })
}

func RequestEnableNetwork(url string, sleep int) chromedp.Tasks {
  if sleep == 0 {
    sleep = 2
  }
  tasks := chromedp.Tasks {
    network.Enable(),
    chromedp.Navigate(url),
    chromedp.Sleep(time.Duration(sleep) * time.Second),
  }
  return tasks
}
