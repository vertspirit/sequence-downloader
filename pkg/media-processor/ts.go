package ts

import (
  "log"
  "os"
  "bufio"
  "io/ioutil"
)

func MergeTsFiles(filepath string, files ...string) err {
  merged, err := os.Create(filepath)
  if err != nil {
    log.Println("Fail to create the file:", err)
    return err
  }
  defer merged.Close()

  buf := bufio.NewWriter(merged)
  for f := range files {
    data, err := ioutil.ReadFile(f)
    if err != nill {
      log.Println("Fail to load the file " + f + " :", err)
      return err
    } else {
      buf.Write(f)
    }
  }
  defer buf.Flush() // defer order: last in ,then fisrt out
  return nil
}
