package gui

import (
  "runtime"
  "os"
  "os/exec"
  "strings"

  "github.com/therecipe/qt/gui"
  "github.com/therecipe/qt/core"
  "github.com/therecipe/qt/widgets"
)

// enable the high dpi scaling
func EnableHighDpiScaling() {
  core.QCoreApplication_SetAttribute(core.Qt__AA_EnableHighDpiScaling, true)
}

func CreateQApp() *widgets.QApplication {
  app := widgets.NewQApplication(len(os.Args), os.Args)
  return app
}

func CreateMainWindow(title, icon string, x, y, minx, miny, w, h int) *widgets.QMainWindow {
  window := widgets.NewQMainWindow(nil, 0)
  window.SetWindowTitle(title)
  window.SetGeometry2(x, y, w, h)

  if minx != 0 && miny != 0 {
    window.SetMinimumSize2(minx, miny)
  }
  if icon != "" {
    window.SetWindowIcon(gui.NewQIcon5(icon))
  }

  return window
}

func CenterMainWindow(window *widgets.QMainWindow) {
  getPosition := window.FrameGeometry()
  centerPoint := widgets.NewQDesktopWidget().AvailableGeometry2(window).Center()
  getPosition.MoveCenter(centerPoint)
  window.Move(getPosition.TopLeft())
}

func CenterMainWindow2(window *widgets.QMainWindow) {
  getPosition := window.FrameGeometry()
  h := getPosition.Height()
  w := getPosition.Width()
  screenSize := widgets.NewQDesktopWidget().ScreenGeometry2(window)
  sh := screenSize.Height()
  sw := screenSize.Width()
  x := (sw / 2) - (w / 2)
  y := (sh / 2) - (h / 2)
  window.SetGeometry2(x, y, w, h)
}

// Customize the default close event for main window
// QMainWindow.ConnectCloseEvent(MainWindowCloseEvent)
func MainWindowCloseEvent(event *gui.QCloseEvent) {
  dialog := widgets.QMessageBox_Question(
    nil,
    "Exit",
    "Do you want to quit now?",
    widgets.QMessageBox__Yes|widgets.QMessageBox__No,
    widgets.QMessageBox__Yes,
  )
  if dialog == widgets.QMessageBox__Yes {
    event.Accept()
  } else {
    event.Ignore()
  }
}

func GetSystemLocale() (string, string) {
  var (
    locale  string = "en_US"
    lang    string = "en"
  )
  osSystem := runtime.GOOS

  switch osSystem {
  case "linux":
    envLANG, ok := os.LookupEnv("LANG")
    if ok {
      locale = strings.Split(strings.TrimSpace(envLANG), ".")[0]
      lang = strings.Split(locale, "_")[0]
    }
  case "windows":
    cmd := exec.Command("powershell", "Get-Culture | select -exp Name")
    if stdout, err := cmd.Output(); err != nil {
      locale = strings.TrimSpace(string(stdout))
      lang = strings.Split(locale, "-")[0]
    }
  case "darwin":
    cmd := exec.Command("sh", "osascript -e 'user locale of (get system info)'")
    if stdout, err := cmd.Output(); err != nil {
      locale = strings.TrimSpace(string(stdout))
      lang = strings.Split(locale, "_")[0]
    }
  }

  return locale, lang
}

func CreateTranslator() *core.QTranslator {
  translator := core.NewQTranslator(nil)
  return translator
}

func SetAppLocaleTranslator(qm, locale string) {
  if locale == "" {
    locale, _ = GetSystemLocale()
  }
  translator := core.NewQTranslator(nil)
  translator.Load2(core.NewQLocale2(locale), "translation", "_", ":/i18n", ".qm")
  core.QCoreApplication_InstallTranslator(translator)
}
