package gui

import (
  "errors"

  "github.com/therecipe/qt/widgets"
  "github.com/therecipe/qt/gui"
  "github.com/therecipe/qt/core"
)

type WidgetSettings struct {
  Title         string                    // SetWindowTitle()
  Text          string                    // SetText()
  Icon          string                    // SetIcon(gui.NewQIcon5(Icon string))
  Picture       string                    // SetPicture()
  Pixmap        string                    // SetPixmap()
  Movie         string                    // SetMovie()
  NumberFloat   float64                   // SetNum2(num float64)
  NumberInt     int                       // SetNum(num int)
  Indent        int
  MarginV       int                       // for QLabel (vertical margin)
  MarginH       int                       // horizontal margin
  MarginLeft    int
  MarginTop     int
  MarginRight   int
  MarginBottom  int
  Alignment     core.Qt__AlignmentFlag    // SetAlignment(core.Qt__AlignVCenter)
}

// BoxLayout (vertical or horizontal)
func (w *WidgetSettings) CreateBoxLayout(parent widgets.QWidget_ITF, orient string, stretch int) (*widgets.QWidget, *widgets.QBoxLayout, error) {
  var (
    err  error
    bl   *widgets.QBoxLayout
  )
  box := widgets.NewQWidget(parent, 0) // a container for widgets with layout
  switch orient {
  case "horizontal":
    // QBoxLayout__LeftToRight (0), QBoxLayout__RightToLeft (1)
    bl = widgets.NewQBoxLayout(0, box)
  case "vertical":
    // QBoxLayout__TopToBottom (2), QBoxLayout__BottomToTop (3)
    bl = widgets.NewQBoxLayout(2, box)
  default:
    err = errors.New("The orient could be only vertical or horizontal.")
    return box, bl, err
  }
  if stretch != 0 {
    bl.AddStretch(stretch)
  }

  if w.MarginLeft != 0 || w.MarginTop != 0 || w.MarginRight != 0 || w.MarginBottom != 0 {
    margin := core.NewQMargins2(w.MarginLeft, w.MarginTop, w.MarginRight, w.MarginBottom)
    box.SetContentsMargins2(margin)
  }
  return box, bl, nil
}

func BoxLayoutToHBoxLayout(bl *widgets.QBoxLayout) *widgets.QHBoxLayout {
  return &widgets.QHBoxLayout {
    QBoxLayout: *bl, // SetSpacing(5), SetMargin(2)
  }
}

func BoxLayoutToVBoxLayout(bl *widgets.QBoxLayout) *widgets.QVBoxLayout {
  return &widgets.QVBoxLayout {
    QBoxLayout: *bl,
  }
}

func (w *WidgetSettings) CreateSimpleBoxLayout(orient string) (*widgets.QWidget, error) {
  var (
    err  error
    bl   interface{}
  )
  box := widgets.NewQWidget(nil, 0)
  switch orient {
  case "vertical":
    bl = widgets.NewQVBoxLayout()
  case "horizontal":
    bl = widgets.NewQHBoxLayout()
  default:
    err = errors.New("The orient could be only vertical or horizontal.")
    return box, err
  }
  box.SetLayout(bl.(widgets.QLayout_ITF))

  if w.MarginLeft != 0 || w.MarginTop != 0 || w.MarginRight != 0 || w.MarginBottom != 0 {
    margin := core.NewQMargins2(w.MarginLeft, w.MarginTop, w.MarginRight, w.MarginBottom)
    box.SetContentsMargins2(margin)
  }

  return box, nil
}

// Label
func (w *WidgetSettings) CreateLabel(parent widgets.QWidget_ITF, shape int, text, image string) *widgets.QLabel {
  label := widgets.NewQLabel(parent, 0)
  if w.Alignment != 0 {
    label.SetAlignment(w.Alignment)
  }
  if w.Indent != 0 {
    label.SetIndent(w.Indent)
  }
  if w.MarginV != 0 {
    label.SetMargin(w.MarginV)
  }
  // Label can't use SetPixmap() and SetText() at the same time.
  if image != "" {
    label.SetPixmap(gui.NewQPixmap3(image, "", 0))
  } else if text != "" {
    label.SetText(text)
  }
  switch shape {
  case 1:
    label.SetFrameShape(widgets.QFrame__Box)
  case 2:
    label.SetFrameShape(widgets.QFrame__Panel)
  case 3:
    label.SetFrameShape(widgets.QFrame__WinPanel)
  case 4:
    label.SetFrameShape(widgets.QFrame__HLine)
  case 5:
    label.SetFrameShape(widgets.QFrame__VLine)
  case 6:
    label.SetFrameShape(widgets.QFrame__StyledPanel)
  default:
    // QFrame__NoFrame => 0
    label.SetFrameShape(widgets.QFrame__NoFrame)
  }
  return label
}

// MenuBar
func CreateMebuBar(parent widgets.QWidget_ITF) *widgets.QMenuBar {
  mbar := widgets.NewQMenuBar(parent)
  return mbar
}

func CreateMenu(mbar *widgets.QMenuBar, name string, icon string) *widgets.QMenu {
  var menu *widgets.QMenu
  if icon == "" {
    menu = mbar.AddMenu2(name)
  } else {
    menu = mbar.AddMenu3(gui.NewQIcon5(icon), name)
  }
  return menu
}

func AddMenuAction(menu *widgets.QMenu, item, icon, shortcut, tip string, f func(bool)) {
  var act *widgets.QAction
  if icon == "" {
    act = menu.AddAction(item)
  } else {
    act = menu.AddAction2(gui.NewQIcon5(icon), item)
  }
  if shortcut != "" {
    act.SetShortcut(gui.NewQKeySequence2(shortcut, gui.QKeySequence__NativeText))
  }
  if tip != "" {
    act.SetStatusTip(tip)
  }
  act.ConnectTriggered(f)
}

func AddMenuSeparator(menu *widgets.QMenu) {
  menu.AddSeparator()
}

func AddMenuSetion(menu *widgets.QMenu, text, icon string) {
  if icon == "" {
    menu.AddSection(text)
  } else {
    menu.AddSection2(gui.NewQIcon5(icon), text)
  }
}

// Dialogs
func (w *WidgetSettings) CreateAboutMessageBox(parent widgets.QWidget_ITF) {
  var msgbox *widgets.QMessageBox
  msgbox = widgets.NewQMessageBox(parent)
  if w.Title != "" {
    msgbox.SetWindowTitle(w.Title)
  }
  if w.Text != "" {
    msgbox.SetText(w.Text)
  }
  msgbox.SetIcon(widgets.QMessageBox__NoIcon)
  msgbox.About(parent, "&about", w.Text)
}

func (w *WidgetSettings) CreateAboutQtMessageBox(parent widgets.QWidget_ITF) {
  var msgbox *widgets.QMessageBox
  msgbox = widgets.NewQMessageBox(parent)
  if w.Title != "" {
    msgbox.SetWindowTitle(w.Title)
  }
  if w.Text != "" {
    msgbox.SetText(w.Text)
  }
  msgbox.SetIcon(widgets.QMessageBox__NoIcon)
  msgbox.AboutQt(parent, "about &Qt")
}

func (w *WidgetSettings) CreateInfoMessageBox(parent widgets.QWidget_ITF) {
  var msgbox *widgets.QMessageBox
  var btns   widgets.QMessageBox__StandardButton
  msgbox = widgets.NewQMessageBox(parent)
  msgbox.SetIcon(widgets.QMessageBox__Information)
  btns = msgbox.Information(parent, w.Title, w.Text, 0, 0)
  msgbox.SetDefaultButton2(btns)
}

func (w *WidgetSettings) CreateQuestionMessageBox(parent widgets.QWidget_ITF) {
  msgbox := widgets.NewQMessageBox(parent)
  msgbox.SetIcon(widgets.QMessageBox__Question)
  btns := msgbox.Question(parent, w.Title, w.Text, 0, 0)
  msgbox.SetDefaultButton2(btns)
}

func (w *WidgetSettings) CreateWarningMessageBox(parent widgets.QWidget_ITF) {
  msgbox := widgets.NewQMessageBox(parent)
  msgbox.SetIcon(widgets.QMessageBox__Warning)
  btns := msgbox.Warning(parent, w.Title, w.Text, 0, 0)
  msgbox.SetDefaultButton2(btns)
}

func (w *WidgetSettings) CreateCriticalMessageBox(parent widgets.QWidget_ITF) {
  msgbox := widgets.NewQMessageBox(parent)
  msgbox.SetIcon(widgets.QMessageBox__Critical)
  btns := msgbox.Critical(parent, w.Title, w.Text, 0, 0)
  msgbox.SetDefaultButton2(btns)
}

func (w *WidgetSettings) CreateDialog(parent widgets.QWidget_ITF, wtype core.Qt__WindowType) *widgets.QDialog {
  // core => type Qt__WindowType int64
  var dg *widgets.QDialog
  if wtype != 0 {
    dg = widgets.NewQDialog(parent, wtype)
  } else {
    dg = widgets.NewQDialog(parent, core.Qt__Dialog)
  }
  dg.SetModal(true)
  dg.Show()
  return dg
}

func (w *WidgetSettings) CreateFileDialog(parent widgets.QWidget_ITF, caption, dir, filter string) *widgets.QFileDialog {
  // filter: "All Files (*);;Text Files (*.txt)"
  fd := widgets.NewQFileDialog2(parent, caption, dir, filter)
  fd.SetConfirmOverwrite(true)
  fd.SetFileMode(1)
  //fd.SetDirectory(directory)
  //fd.SetFilter(filter)
  //fd.SetHistory(paths []string)
  return fd
}

// Buttons
func CreateGroupVBox(parent widgets.QWidget_ITF, title string, stretch int, spacing int, items ...widgets.QWidget_ITF) *widgets.QGroupBox {
  gbox := widgets.NewQGroupBox(parent)
  if title != "" {
    gbox.SetTitle(title)
  }
  vbox := widgets.NewQVBoxLayout2(parent)
  for _, item := range items {
    vbox.AddWidget(item, stretch, 32)
  }
  if spacing != 0 {
    vbox.SetSpacing(spacing)
  }
  gbox.SetLayout(vbox)
  return gbox
}

func CreateGroupHBox(parent widgets.QWidget_ITF, title string, stretch int, spacing int, items ...widgets.QWidget_ITF) *widgets.QGroupBox {
  gbox := widgets.NewQGroupBox(parent)
  if title != "" {
    gbox.SetTitle(title)
  }
  hbox := widgets.NewQHBoxLayout2(parent)
  hbox.AddSpacing(12)
  for _, item := range items {
    hbox.AddWidget(item, stretch, 1)
  }
  hbox.AddSpacing(12)
  if spacing != 0 {
    hbox.SetSpacing(spacing)
  }
  gbox.SetLayout(hbox)
  return gbox
}

func CreateRadioGroupVBox(parent widgets.QWidget_ITF, title string, labels ...string) *widgets.QGroupBox {
  gbox := widgets.NewQGroupBox(parent)
  if title != "" {
    gbox.SetTitle(title)
  }
  vbox := widgets.NewQVBoxLayout2(parent)
  for _, l := range labels {
    radiobtn := widgets.NewQRadioButton2(l, parent)
    vbox.AddWidget(radiobtn, 0, 32)
  }
  gbox.SetLayout(vbox)
  return gbox
}

func CreateRadioButton(parent widgets.QWidget_ITF, label string) *widgets.QRadioButton {
  var rbtn *widgets.QRadioButton
  if label != "" {
    rbtn = widgets.NewQRadioButton2(label, parent)
  } else {
    rbtn = widgets.NewQRadioButton(parent)
  }
  return rbtn
}

func CreateRadioButtonGroup(parent widgets.QWidget_ITF, clicked func(id int), exclusive bool, rbtns ...*widgets.QRadioButton) *widgets.QButtonGroup {
  var btngp *widgets.QButtonGroup
  for i, btn := range rbtns {
    btngp.AddButton(btn, i) // AddButton(button QAbstractButton_ITF, id int)
    // btngp.RemoveButton(button QAbstractButton_ITF)
  }
  btngp.SetExclusive(exclusive) // set group to be mutually exclusive or not
  btngp.ConnectButtonClicked2(clicked) // ButtonClicked2(id int)
  return btngp
}

func CreateCheckBox(parent widgets.QWidget_ITF, label string) *widgets.QCheckBox {
  var cbox *widgets.QCheckBox
  if label != "" {
    cbox = widgets.NewQCheckBox2(label, parent)
  } else {
    cbox = widgets.NewQCheckBox(parent)
  }
  return cbox
  // cbox.CheckState() => 0 Unchecked, 1 PartiallyChecked, 2 Checked
}

func CreateComboBox(parent widgets.QWidget_ITF, items ...string) *widgets.QComboBox {
  combobtn := widgets.NewQComboBox(parent)
  for _, item := range items {
    combobtn.AddItem(item, core.NewQVariant15(item))
  }
  return combobtn
  // combobtn.CurrentText() => get the current selected text
}

func SetupComboButtonAction(combobtn *widgets.QComboBox, selected func(text string)) {
  combobtn.ConnectActivated2(selected)
}

func CreateSpinBox(parent widgets.QWidget_ITF, min, max, step, def int, prefix, suffix string) *widgets.QSpinBox {
  spin := widgets.NewQSpinBox(parent)
  spin.SetRange(min, max)
  // SetMinimum() default is 0
  // SetMaximum() default is 99

  spin.SetValueDefault(def)
  spin.TextFromValueDefault(def)

  if step <= 0 {
    spin.SetSingleStep(1)
  } else {
    spin.SetSingleStep(step)
  }

  if prefix != "" {
    spin.SetPrefix(prefix)
  }
  if suffix != "" {
    spin.SetSuffix(suffix)
  }
  return spin
}

func CreatePushButton(parent widgets.QWidget_ITF, text string, clicked func(bool)) *widgets.QPushButton {
  btn := widgets.NewQPushButton(parent)
  if text != "" {
    btn.SetText(text)
  }
  if clicked != nil {
    btn.ConnectClicked(clicked) // DisconnectClicked()
  }
  return btn
}

// LineEdit
func CreateNewQIntValidator(parent widgets.QWidget_ITF, min, max int) *gui.QIntValidator {
  var vld *gui.QIntValidator
  if max == 0 && min == 0 {
    vld = gui.NewQIntValidator(parent)
  } else {
    vld = gui.NewQIntValidator2(min, max, parent)
  }
  return vld
}

func CreateRegExpValidator(parent widgets.QWidget_ITF, pattern string) *gui.QRegularExpressionValidator {
  reg := core.NewQRegularExpression()
  reg.SetPattern(pattern)
  vld := gui.NewQRegularExpressionValidator2(reg, parent)
  return vld
}
