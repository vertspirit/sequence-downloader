package main

import (
  "flag"
  "testing"

  "sequence-downloader/pkg/logger"
)

type FileLogger = logger.FileLogger

var (
  paramLogfile  string
  paramLogger   bool
)

func init() {
  // to fix the issue about 'golang 13 changed the order of the test initializer' that causes 'flag provided but not defined: -test.timeout'
  var _ = func() bool {
    testing.Init()
    return true
  }()

  flag.StringVar(&paramLogfile, "log-file", "history.log", "The file path for logging error message.")
  flag.StringVar(&paramLogfile, "f", "history.log", "The file path for logging. (shorten)")
  flag.BoolVar(&paramLogger, "enable-log", true, "Enable logger for history, default is true.")
  flag.BoolVar(&paramLogger, "g", true, "Enable logger for history, default is true. (shorten)")
  flag.Parse()

  if paramLogger {
    logger := &FileLogger {
      File: paramLogfile,
      Name: "sequence-downloader",
    }
    logger.Setup()
  }
}
