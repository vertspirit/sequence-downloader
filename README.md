# Sequence-Downloader

This is a simple GUI tool for sequence downloading. It's useful for downloading multiple ts files, and combine files to single ts file.

## Build

### GNU/Linux Environment

Run the following command to install basic pacakges for building:

```bash
sudo apt-get -y install build-essential libglu1-mesa-dev libpulse-dev libglib2.0-dev
```

Download and install proper version Qt from official website:

```bash
wget https://download.qt.io/archive/qt/5.13/5.13.2/qt-opensource-linux-x64-5.13.2.run
chmod +x qt-opensource-linux-x64-5.13.2.run
```

Set the environment variables for Qt:

```bash
export QT_VERSION=5.13.2
export QT_QMAKE_DIR=/opt/Qt5.13.2/5.13.2/gcc_64/bin/
export QT_DIR=/opt/Qt5.13.2
echo "export QT_VERSION=5.13.2" | sudo tee -a /etc/profile > /dev/null
echo "export QT_QMAKE_DIR=/opt/Qt5.13.2/5.13.2/gcc_64/bin/" | sudo tee -a /etc/profile > /dev/null
echo "export QT_DIR=/opt/Qt5.13.2" | sudo tee -a /etc/profile > /dev/null
```

Install the latest golang package via snap:

```bash
sudo snap install go --classic
```

Disable the go module before run qtsetup:

```bash
go env -w GO111MODULE=off
```

Download and build the therecipe/qt:

```bash
mkdir ~/go-qt && cd ~/go-qt
go get -u -v github.com/therecipe/qt/cmd/...
```

Run the qtsetup to prepare the Qt build environment:

```bash
$(go env GOPATH)/bin/qtsetup
```

Create the alias for Qt deploy binary:

```bash
echo "alias qtrcc=\"$(go env GOPATH)/bin/qtrcc\"" >> ~/.bash_aliases
echo "alias qtmoc=\"$(go env GOPATH)/bin/qtmoc\"" >> ~/.bash_aliases
echo "alias qtminimal=\"$(go env GOPATH)/bin/qtminimal\"" >> ~/.bash_aliases
echo "alias qtdeploy=\"$(go env GOPATH)/bin/qtdeploy\"" >> ~/.bash_aliases
```

Run the following command to compile the sequence-downloader binary:

```bash
cd sequence-downloader/
export GO111MODULE=off
qtmoc
qtrcc
export GO111MODULE=on
go build -mod=mod -o sequence-downloader
```

## License

This application is releaded under MIT License.

Qt binding for Golang (github.com/therecipe/qt) licensed under [LGPLv3](https://opensource.org/licenses/LGPL-3.0).

Qt itself is licensed under multiple [licenses](https://www.qt.io/licensing).

Package chromedp licensed under [MIT](https://github.com/chromedp/chromedp/blob/master/LICENSE).
