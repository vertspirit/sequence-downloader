package main

import (
  "fmt"
)

var (
  AppName  string = "sequence-downloader"
  Version  string
  Build    string
)

func main() {

  if Version != "" {
    fmt.Printf("[%s] Version: %s\n", AppName, Version)
  }
  if Build != "" {
    fmt.Printf("[%s] Build Date: %s\n", AppName, Build)
  }

  StartUI()
}
